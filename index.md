---
layout: default
title: index
order: 1
---

# LCSB Jekyll Theme

Welcome to our LCSB Jekyll theme! Jekyll is a *static webpages generator*, which turns markdown files into a website. Coupled with Gitlab-CI, it can generate and host the pages for you in a completely automatic way.

This repository contains a template, that you should use when creating any webpage for your LCSB-related projects. The layout has been already approved by our Communications Department, so that putting the website live will be fast and easy!

> Note, that unless you want to work directly on the theme, you should not use/clone this repository (`jekyll-theme-lcsb-default`) but use `pages-jekyll-lcsb-template` instead.

# How to use?
To start a new page for your repository, you should fork [this project](https://git-r3lab.uni.lu/core-services/pages-jekyll-lcsb-template) and modify its contents directly.
The webpage will be published automatically under uni.lu network. 

For more information, refer to [https://git-r3lab.uni.lu/core-services/pages-jekyll-lcsb-template/blob/master/index.md](https://git-r3lab.uni.lu/core-services/pages-jekyll-lcsb-template/blob/master/index.md).

 > Make sure that you have Ruby in at least 2.4 version.

## Contributing
Bug reports and pull requests are welcome on Gitlab at [https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default/issues](https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default/issues). 

This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

# Development
Please see [https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default/blob/master/development.md](https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default/blob/master/development.md).

# Contact
In case you need any information or help, don't hesitate to contact [LCSB Sysadmins Team (lcsb-sysadmins@uni.lu)](mailto:lcsb-sysadmins@uni.lu).

# License
The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
